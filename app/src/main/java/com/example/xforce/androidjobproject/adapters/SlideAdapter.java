package com.example.xforce.androidjobproject.adapters;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.example.xforce.androidjobproject.R;
import com.firebase.ui.storage.images.FirebaseImageLoader;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

import java.util.ArrayList;
import java.util.List;

public class SlideAdapter extends PagerAdapter {

    //a DetailsActivityben megnyitott kepek teljes kepernyon megjelenitve
    private List<String> images;
    private LayoutInflater inflater;
    private Context context;

    public SlideAdapter(Context context,List<String> images ) {
        this.images = images;
        this.context = context;
        inflater = LayoutInflater.from(context);
    }

    @Override
    public void destroyItem(@NonNull ViewGroup container, int position, @NonNull Object object) {
        container.removeView((View) object);
    }

    @Override
    public int getCount() {
        return images.size();
    }

    @Override
    public boolean isViewFromObject(@NonNull View view, @NonNull Object o) {
        return view.equals(o);
    }
    @NonNull
    @Override
    public Object instantiateItem(ViewGroup view, int position) {
        View imageLayout = inflater.inflate(R.layout.slide_layout, view, false);

        assert imageLayout != null;
        final ImageView imageView =imageLayout.findViewById(R.id.image);
        StorageReference mStorageRef = FirebaseStorage.getInstance().getReference().child("images").child(images.get(position));
        //a postban mentett fotokat betolti a SlideView-ba
        Glide.with(context)
                .using(new FirebaseImageLoader())
                .load(mStorageRef)
                .into(imageView);

        view.addView(imageLayout);

        return imageLayout;
    }
}
