package com.example.xforce.androidjobproject.layout;

import android.content.Context;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Handler;
import android.os.Looper;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;

import com.example.xforce.androidjobproject.R;

import java.util.UUID;


public class MyFrameLayout extends FrameLayout {

    // a kepeket jeleniti meg a szerkesztoben es azok torleset teszi lehetove
    // nem minden kepet kell ujra feltolteni ezert amelyiket az adatbazisbol toltotte le
    // (post szerkesztese) nem tolti le ujra
    private static final int RC_IMAGE = 1;
    private ImageView imageView;
    private ImageButton deleteButton;
    private Uri imageUri;
    private String filename;
    private boolean toUpload;


    public MyFrameLayout(@NonNull Context context) {
        this(context,null);
    }

    public MyFrameLayout(@NonNull Context context, @Nullable AttributeSet attrs) {
        this(context,attrs,0);
    }

    public MyFrameLayout(@NonNull Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        toUpload = true;
        inflate(context, R.layout.my_frame_layout,this);
        imageView = findViewById(R.id.base_image_view);
        deleteButton = findViewById(R.id.delete_button);
        //torles gomb egy animaciot indit el
        deleteButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                MyFrameLayout.this.startAnimation(AnimationUtils.loadAnimation(getContext(),R.anim.deleted_picture_animation));
            }
        });
    }

    //a torles animacio vegen torli a megfelelo objektuom
    @Override
    protected void onAnimationEnd() {
        new Handler(Looper.getMainLooper()).post(new Runnable() {
            @Override
            public void run() {
                ViewGroup parent = (ViewGroup) getParent();
                parent.removeView(MyFrameLayout.this);
            }
        });
        super.onAnimationEnd();

    }

    //kell e menteni az adatbazisba az adott kepet
    public boolean isToUpload() {
        return toUpload;
    }

    public void setBitmap(Bitmap bitmap)
    {
        toUpload = false;
        this.imageView.setImageBitmap(bitmap);
    }

    public void setFilename(String filename) {
        this.filename = filename;
    }

    public String getFilename()
    {
        return filename;
    }

    public Uri getImageUri() {
        return imageUri;
    }

    public void setImageUri(Uri uri)
    {
        imageUri = uri;
        filename = UUID.randomUUID().toString();
        imageView.setImageURI(uri);
    }

}
