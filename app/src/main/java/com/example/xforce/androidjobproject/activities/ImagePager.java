package com.example.xforce.androidjobproject.activities;

import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.example.xforce.androidjobproject.R;
import com.example.xforce.androidjobproject.adapters.SlideAdapter;

import java.util.List;

public class ImagePager extends AppCompatActivity {
    private List<String> images;
    private static ViewPager mPager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_image_pager);
        images = (List<String>) getIntent().getSerializableExtra("DATA");


        mPager = findViewById(R.id.pager);
        mPager.setAdapter(new SlideAdapter(ImagePager.this, images));
    }
}
