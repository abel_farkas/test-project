package com.example.xforce.androidjobproject.activities;

import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.example.xforce.androidjobproject.R;
import com.example.xforce.androidjobproject.model.Post;
import com.firebase.ui.storage.images.FirebaseImageLoader;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

import java.io.Serializable;
import java.util.List;
import java.util.Objects;

public class DetailsActivity extends AppCompatActivity {

    private TextView textTitle, textDetails;
    private Button btnPhone, btnEmail;
    private LinearLayout photoLayout;
    private List<String> fileNames;
    private DatabaseReference actualPostDatabaseReference;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_details);

        Intent mainIntent = getIntent();
        Post data = (Post) mainIntent.getSerializableExtra("data");

        textTitle = findViewById(R.id.text_title);
        textDetails = findViewById(R.id.text_details);
        photoLayout = findViewById(R.id.photo_layout);
        btnPhone = findViewById(R.id.btn_phone);
        btnEmail = findViewById(R.id.btn_email);

        textTitle.setText(data.getTitle());
        textDetails.setText(data.getDetails());
        btnPhone.setText(data.getPhone());
        btnEmail.setText(data.getEmail());
        fileNames = data.getPhotoUrl();

        //telefon hivas
        btnPhone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent callIntent = new Intent(Intent.ACTION_DIAL);
                callIntent.setData(Uri.parse("tel:" + btnPhone.getText()));
                startActivity(callIntent);
            }
        });
        //email kuldes
        btnEmail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent emailIntent = new Intent(Intent.ACTION_SENDTO, Uri.fromParts(
                        "mailto", btnEmail.getText().toString(), null));
                startActivity(Intent.createChooser(emailIntent, "Send email..."));
            }
        });

        actualPostDatabaseReference = FirebaseDatabase.getInstance().getReference().getRoot().child( mainIntent.getStringExtra("ID"));

        //kepek betoltese firebase storage-bol
        for (String i : fileNames) {
            StorageReference mStorageRef = FirebaseStorage.getInstance().getReference().child("images").child(i);

            ImageView newImage = new ImageView(this);

            Glide.with(this.getBaseContext())
                    .using(new FirebaseImageLoader())
                    .load(mStorageRef)
                    .into(newImage);

            newImage.setClickable(true);
            newImage.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent pagerIntent = new Intent(DetailsActivity.this, ImagePager.class);
                    pagerIntent.putExtra("DATA", (Serializable) fileNames);
                    startActivity(pagerIntent);

                }
            });

            photoLayout.addView(newImage);
            final float scale = getResources().getDisplayMetrics().density;
            newImage.getLayoutParams().width = (int) (127 * scale);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        getMenuInflater().inflate(R.menu.details_menu, menu);
        setTitle("Details");
        Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setBackgroundDrawable(new ColorDrawable(getResources().getColor(R.color.colorAccent, this.getTheme())));

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        //menugombok lekezelese
        switch (item.getItemId()) {
            //vissza gomb
            case android.R.id.home:
                //bezarodik az activity
                this.finish();
                return true;
            //report gomb
            case R.id.btn_report:
                //torli az aktualis postot
                actualPostDatabaseReference.removeValue();
                //felugro szoveg: a post torolve lett
                Toast.makeText(DetailsActivity.this, "This post has been removed", Toast.LENGTH_SHORT).show();
                //var 2 masodpercet utana bezarja az activity-t
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        DetailsActivity.this.finish();
                    }
                }, 2000);
                return true;
            //share gomb
            case R.id.btn_share:
                //alapertelmezett megoszto applikacionak tovabbitja a cimet, a reszleteket es egy linket az "applikaciohoz"
                Intent sendIntent = new Intent();
                sendIntent.setAction(Intent.ACTION_SEND);
                sendIntent.putExtra(Intent.EXTRA_SUBJECT, textTitle.getText());
                sendIntent.putExtra(Intent.EXTRA_TEXT, textDetails.getText() + "\n You can find our app here:\n www.testprojekt.com/downloads");
                sendIntent.setType("text/plain");
                startActivity(sendIntent);
                return true;

            default:
                return true;
        }
    }
}
