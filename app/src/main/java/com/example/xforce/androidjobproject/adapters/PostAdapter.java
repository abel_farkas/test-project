package com.example.xforce.androidjobproject.adapters;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.example.xforce.androidjobproject.activities.DetailsActivity;
import com.example.xforce.androidjobproject.activities.PostEditorActivity;
import com.example.xforce.androidjobproject.R;
import com.example.xforce.androidjobproject.model.Post;
import com.firebase.ui.storage.images.FirebaseImageLoader;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

public class PostAdapter extends RecyclerView.Adapter<PostAdapter.MyViewHolder> implements Filterable {

    // RecycleView post adapter
    // a postok betolteset vegzi a RecycleView-ba
    // a posztok szureset vegzi

    private Map<String, Object> fPostMap;
    private Map<String, Object> postMap;
    //ha az adott post-ban tarolt userID megegyezik a felhasznaloeval akkor
    //torolheti es szerkesztheti a postokat
    //kulomben megnyithatja a DetailsActivity-ben
    private String userID;

    //postok szurese
    @Override
    public Filter getFilter() {
        return new Filter() {
            CharSequence charSequence;

            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {
                this.charSequence = charSequence;
                String filter = charSequence.toString();
                //ures karakterlancra megjelenik minden
                if (filter.length() == 0) {
                    fPostMap = postMap;
                } else {
                    Map<String,Object> filteredMap = new LinkedHashMap<>();
                    for (Map.Entry<String, Object> i : postMap.entrySet()) {
                        //szures cim szerint
                        if (((Post) i.getValue()).getTitle().toLowerCase().contains(filter.toLowerCase())) {
                            filteredMap.put(i.getKey(), i.getValue());
                        }
                    }
                    fPostMap = filteredMap;

                }
                FilterResults filterResults = new FilterResults();
                filterResults.values = fPostMap;
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                fPostMap = (Map<String, Object>) filterResults.values;
                notifyDataSetChanged();
            }
        };
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView title, details;
        public ImageView thumbnail;
        public Button btnDelete, btnDetails;
        public View itemView;
        public String ID;
        public Post data;


        public MyViewHolder(@NonNull final View itemView) {
            super(itemView);
            this.itemView = itemView;
            title = itemView.findViewById(R.id.text_title);
            details = itemView.findViewById(R.id.text_details);
            thumbnail = itemView.findViewById(R.id.image_thumbnail);
            btnDelete = itemView.findViewById(R.id.btn_delete);
            btnDetails = itemView.findViewById(R.id.btn_details);

            btnDelete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    FirebaseDatabase.getInstance().getReference().getRoot().child(ID).removeValue();
                    fPostMap.remove(ID);
                    postMap.remove(ID);
                    notifyDataSetChanged();
                }
            });
            btnDetails.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (btnDetails.getText().equals("Details")) {
                        Intent detailsIntent = new Intent(itemView.getContext(), DetailsActivity.class);
                        detailsIntent.putExtra("data", (Serializable) data);
                        detailsIntent.putExtra("ID", ID);
                        itemView.getContext().startActivity(detailsIntent);
                    } else {
                        Intent editorIntent = new Intent(itemView.getContext(), PostEditorActivity.class);
                        editorIntent.putExtra(PostEditorActivity.EDITOR_MODE, PostEditorActivity.EDIT_MODE);
                        editorIntent.putExtra(PostEditorActivity.DATA, data);
                        editorIntent.putExtra(PostEditorActivity.KEY, ID);
                        editorIntent.putExtra(PostEditorActivity.USER_ID, data.getUserID());
                        itemView.getContext().startActivity(editorIntent);
                    }
                }
            });
        }
    }

    public PostAdapter(Map<String, Object> postMap, String userID) {
        this.postMap = postMap;
        this.fPostMap = postMap;
        this.userID = userID;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.post_display, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        List<Object> tmpList = new ArrayList<>(fPostMap.values());
        List<String> keyList = new ArrayList<>(fPostMap.keySet());
        Post data = (Post) tmpList.get(position);
        holder.data = data;
        holder.ID = keyList.get(position);

        StorageReference mStorageRef = FirebaseStorage.getInstance().getReference().child("images/" + data.getPhotoUrl().get(0));
        //a postban mentett fotok kozul az elsot betolti thumbnailkent
        Glide.with(holder.itemView.getContext())
                .using(new FirebaseImageLoader())
                .load(mStorageRef)
                .into(holder.thumbnail);


        if (!data.getUserID().equals(userID)) {
            holder.btnDelete.setVisibility(View.GONE);
            holder.btnDetails.setText("Details");
        } else {
            holder.btnDetails.setText("Edit");
            holder.btnDelete.setVisibility(View.VISIBLE);
        }
        holder.title.setText(data.getTitle());
        holder.details.setText(data.getDetails());
    }

    public void addNewElement(String key, Object data) {
        Map<String, Object> tmpMap = new LinkedHashMap<>(postMap);
        postMap = new LinkedHashMap<>();
        postMap.put(key, data);
        postMap.putAll(tmpMap);
    }

    public void changeElement(String key, Object data) {
        postMap.remove(key);
        addNewElement(key, data);
    }

    public void deleteElement(String key) {
        postMap.remove(key);
    }

    public void setPostMap(Map<String, Object> postMap) {
        this.postMap = postMap;
    }

    @Override
    public int getItemCount() {
        return (new ArrayList<>(fPostMap.values())).size();
    }
}
