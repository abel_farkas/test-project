package com.example.xforce.androidjobproject.activities;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.example.xforce.androidjobproject.R;
import com.example.xforce.androidjobproject.model.Post;
import com.example.xforce.androidjobproject.layout.MyFrameLayout;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FileDownloadTask;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class PostEditorActivity extends AppCompatActivity {

    // uj post letrehozasara/meglevo post szerkesztesere szolgalo activity
    // a mode valtozoban tarolom hogy uj posztot akarok-e letrehozni vagy meglevot szerkeszteni

    public static final String EDITOR_MODE = "EDITOR_MODE";
    public static final String EDIT_MODE = "EDIT_MODE";
    public static final String NEW_ITEM_MODE = "NEW_ITEM_MODE";
    public static final String USER_ID = "USER_ID";
    public static final String DATA = "DATA";
    public static final String KEY = "KEY";
    private static final int RC_IMAGE = 1;

    private EditText title, details, phone, email;
    private LinearLayout photoLayout;
    private ImageButton addImage;
    private DatabaseReference mDatabase;
    private StorageReference mStorageReference;
    private String userID;
    private int mode;
    private int imageCount, imagesUploaded;
    private List<String> fileNames;
    private String key;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_editor);
        //az app bar cimet allitja be annak megfeleloen hogy letrehozni vagy szerkeszteni akarok egy postot
        if (getIntent().getStringExtra(EDITOR_MODE).equals(NEW_ITEM_MODE)) {
            mode = 0;
            setTitle("Add");
        } else if (getIntent().getStringExtra(EDITOR_MODE).equals(EDIT_MODE)) {
            mode = 1;
            setTitle("Edit");
        }

        imageCount = 0;
        imagesUploaded = 0;

        userID = getIntent().getStringExtra(USER_ID);
        key = getIntent().getStringExtra(KEY);
        addImage = findViewById(R.id.btn_add_image);
        photoLayout = findViewById(R.id.photo_layout);

        mStorageReference = FirebaseStorage.getInstance().getReference();
        title = findViewById(R.id.edit_title);
        details = findViewById(R.id.edit_details);
        phone = findViewById(R.id.edit_phone);
        email = findViewById(R.id.edit_email);
        mDatabase = FirebaseDatabase.getInstance().getReference();

        //betolti a szerkeszteni kivant post adatait
        if (mode == 1) {
            Post data = (Post) getIntent().getSerializableExtra(DATA);
            title.setText(data.getTitle());
            details.setText(data.getDetails());
            phone.setText(data.getPhone());
            email.setText(data.getEmail());

            List<String> fileNames = data.getPhotoUrl();
            for (String i : fileNames) {
                addImage(mStorageReference,i,photoLayout,"images");
            }
        }

        //foto hozzaadasa
        addImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent pickPhoto = new Intent(Intent.ACTION_PICK,
                        android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                startActivityForResult(pickPhoto, RC_IMAGE);
            }
        });
    }


    //a kivalasztott foto feldolgozasa
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == RC_IMAGE && resultCode == RESULT_OK) {
            Uri selectedImage = data.getData();
            MyFrameLayout newFrame = new MyFrameLayout(this);
            newFrame.setImageUri(selectedImage);
            photoLayout.addView(newFrame);

        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.editor_menu, menu);
        Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setBackgroundDrawable(new ColorDrawable(getResources().getColor(R.color.colorAccent, this.getTheme())));
        return true;
    }



    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        //menugombok lekezelese
        switch (item.getItemId()) {
            //vissza gomb
            case android.R.id.home:
                //bezarodik az activity
                this.finish();
                return true;
            //mentes gomb
            case R.id.btn_save:
                //a betoltott fotokat gyujti ossze egy listaba
                List<MyFrameLayout> frameList = new ArrayList<>();
                int count = photoLayout.getChildCount();
                for (int i = 0; i < count; i++) {
                    frameList.add((MyFrameLayout) photoLayout.getChildAt(i));
                }

                doUpload(frameList);

            default:
                return true;
        }
    }
    //fotok feltoltese a felhobe
    //majd a post letrehozasa es lementese az adatbazisba
    private void doUpload(List<MyFrameLayout> frameList) {

        if (checkIfValidPost()) {
            fileNames = new ArrayList<>();
            imageCount = frameList.size();
            if (frameList.size() == 0
                    ) {
                Toast.makeText(this, "Please add at least 1 photo", Toast.LENGTH_SHORT).show();
                return;
            }
            for (View i : frameList) {
                MyFrameLayout tmp = (MyFrameLayout) i;
                if (tmp.isToUpload()) {
                    String filename = tmp.getFilename() + "_" + tmp.getImageUri().getLastPathSegment();
                    fileNames.add(filename);
                    StorageReference uploadReference = mStorageReference.child("images/" + filename);
                    uploadReference.putFile(tmp.getImageUri()).addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception e) {
                            Toast.makeText(PostEditorActivity.this, "Error at image upload", Toast.LENGTH_SHORT).show();
                        }
                    }).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                        @Override
                        public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                            imagesUploaded++;
                            if (imagesUploaded == imageCount) {
                                if (imageCount == imagesUploaded) {
                                    Post newPost = createPost(PostEditorActivity.this.fileNames);
                                    doUpload(mDatabase,newPost, key);
                                }
                            }
                        }
                    });
                } else {
                    imagesUploaded++;
                    fileNames.add(tmp.getFilename());
                    if (imageCount == imagesUploaded) {
                        Post newPost = createPost(PostEditorActivity.this.fileNames);
                        doUpload(mDatabase,newPost, key);
                    }
                }
            }
        }
    }

    //Feltolti a post parameterben kapott objektumot az adatbazisba
    //ha a mode valtozo erteke 0 akkor a key parameter erteket figyelmen kivul hagyja
    //kulomben a key parameterrel megadott bejegyzest fogja modositani
    private void doUpload(DatabaseReference mDatabase,Object post, String key) {
        if (mode == 0) {
            mDatabase.push().setValue(post).addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception e) {
                    Toast.makeText(PostEditorActivity.this, "Error: post not saved Please try again later!", Toast.LENGTH_SHORT).show();
                }
            }).addOnCompleteListener(new OnCompleteListener<Void>() {
                @Override
                public void onComplete(@NonNull Task<Void> task) {
                    PostEditorActivity.this.finish();
                }
            });
        } else {
            mDatabase.child(key).setValue(post).addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception e) {
                    Toast.makeText(PostEditorActivity.this, "Error: post not saved Please try again later!", Toast.LENGTH_SHORT).show();
                }
            }).addOnCompleteListener(new OnCompleteListener<Void>() {
                @Override
                public void onComplete(@NonNull Task<Void> task) {
                    PostEditorActivity.this.finish();
                }
            });
        }
    }

    // editorkent hasznaljuk az activity-t
    // letolti a felhobol a path ban megadott helyrol a fileName nevu kepet
    // letrehoz egy MyFrameLayout-ot amit betesz a photoLayout taroloba
    private void addImage(StorageReference mStorageReference,final String fileName,final LinearLayout photoLayout,String path) {
        try {
            final File localFile = File.createTempFile("images", "jpg");
            mStorageReference.child(path).child(fileName)
                    .getFile(localFile).addOnSuccessListener(new OnSuccessListener<FileDownloadTask.TaskSnapshot>() {
                @Override
                public void onSuccess(FileDownloadTask.TaskSnapshot taskSnapshot) {
                    if (localFile.exists()) {
                        Bitmap myBitmap = BitmapFactory.decodeFile(localFile.getAbsolutePath());
                        MyFrameLayout newFrame = new MyFrameLayout(PostEditorActivity.this);
                        newFrame.setBitmap(myBitmap);
                        newFrame.setFilename(fileName);
                        photoLayout.addView(newFrame);
                    }
                }
            });

        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    //visszaterit egy uj Post objektumot az EditText mezok ertekeivel feltoltve
    private Post createPost(List<String> fileNames) {

        String sTitle = title.getText().toString().trim();
        String sDetails = details.getText().toString().trim();
        String sPhone = phone.getText().toString().trim();
        String sEmail = email.getText().toString().trim();

        return new Post(userID,
                sTitle,
                sDetails,
                sPhone,
                sEmail,
                fileNames,
                System.currentTimeMillis());
    }

    //ellenorzi hogy a bemeneti mezok helyesen vannak-e kitoltve ahhoz
    //hogy egy Post objektumot letre lehessen hozni
    //ha nem akkor hibauzenetet ad a felhasznalonak
    private boolean checkIfValidPost() {
        String sTitle = title.getText().toString().trim();
        String sDetails = details.getText().toString().trim();
        String sPhone = phone.getText().toString().trim();
        String sEmail = email.getText().toString().trim();

        if (sTitle.length() == 0) {
            Toast.makeText(this, "Please add a title", Toast.LENGTH_SHORT).show();
            return false;
        }
        if (sDetails.length() == 0) {
            Toast.makeText(this, "Please add details", Toast.LENGTH_SHORT).show();
            return false;
        }
        if (sPhone.length() == 0) {
            Toast.makeText(this, "Please add a phone number", Toast.LENGTH_SHORT).show();
            return false;
        }
        if (sEmail.length() == 0) {
            Toast.makeText(this, "Please add a e-mail", Toast.LENGTH_SHORT).show();
            return false;
        }
        return true;
    }

}
