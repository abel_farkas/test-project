package com.example.xforce.androidjobproject.model;

import com.google.firebase.database.IgnoreExtraProperties;

import java.io.Serializable;
import java.util.List;

@IgnoreExtraProperties

public class Post implements Serializable {

    //Post adatmodell
    public String userID;
    public String title;
    public String details;
    public String phone;
    public String email;
    public List<String> photoUrl;
    public long date;
    public Post() {
    }

    public Post(String userID,String title, String details, String phone, String email, List<String> photoUrl,long date) {
        this.userID = userID;
        this.title = title;
        this.details = details;
        this.phone = phone;
        this.email = email;
        this.photoUrl = photoUrl;
        this.date = date;
    }

    @Override
    public String toString() {
        return "Post{" +
                "userID='" + userID + '\'' +
                ", title='" + title + '\'' +
                ", details='" + details + '\'' +
                ", phone='" + phone + '\'' +
                ", email='" + email + '\'' +
                ", photoUrl=" + photoUrl +
                '}';
    }

    public void setUserID(String userID) {
        this.userID = userID;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setDetails(String details) {
        this.details = details;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setPhotoUrl(List<String> photoUrl) {
        this.photoUrl = photoUrl;
    }

    public String getUserID() {
        return userID;
    }

    public String getTitle() {
        return title;
    }

    public String getDetails() {
        return details;
    }

    public String getPhone() {
        return phone;
    }

    public String getEmail() {
        return email;
    }

    public List<String> getPhotoUrl() {
        return photoUrl;
    }
}
