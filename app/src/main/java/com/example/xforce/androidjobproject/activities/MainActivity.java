package com.example.xforce.androidjobproject.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Toast;


import com.example.xforce.androidjobproject.R;
import com.example.xforce.androidjobproject.adapters.PostAdapter;
import com.example.xforce.androidjobproject.model.Post;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Objects;

public class MainActivity extends AppCompatActivity {

    //post-ok betoltese es megjelenitese

    private ImageButton logOut, add;
    private EditText search;
    private RecyclerView mainView;
    private PostAdapter postAdapter;
    private String userId;
    private Map<String, Object> data;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        setTheme(R.style.Theme_AppCompat_Light_NoActionBar);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar myToolbar = findViewById(R.id.my_toolbar);
        setSupportActionBar(myToolbar);

        userId = getIntent().getStringExtra("USER_ID");
        logOut = findViewById(R.id.btn_logout);
        add = findViewById(R.id.btn_add);
        mainView = findViewById(R.id.main_view);
        search = findViewById(R.id.search);

        data = new LinkedHashMap<>();
        postAdapter = new PostAdapter(data, userId);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        mainView.setLayoutManager(mLayoutManager);
        mainView.setItemAnimator(new DefaultItemAnimator());
        mainView.setAdapter(postAdapter);

        search.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                MainActivity.this.postAdapter.getFilter().filter(charSequence);
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
        //uj post hozzaadasa
        add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent editorIntent = new Intent(MainActivity.this, PostEditorActivity.class);
                editorIntent.putExtra(PostEditorActivity.EDITOR_MODE, PostEditorActivity.NEW_ITEM_MODE);
                editorIntent.putExtra("USER_ID", userId);
                startActivity(editorIntent);
            }
        });
        //kijelentkezes
        logOut.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(MainActivity.this, "Logout", Toast.LENGTH_SHORT).show();
                FirebaseAuth auth = FirebaseAuth.getInstance();
                auth.signOut();
                startActivity(new Intent(MainActivity.this, SignupActivity.class));
                finish();
            }
        });
        //az osszes eddigi post lekerese az adatbazisbol
        DatabaseReference ref = FirebaseDatabase.getInstance().getReference();
        ref.orderByChild("date").addListenerForSingleValueEvent(
                new ValueEventListener() {
                    @Override
                    @SuppressWarnings({"unchecked"})
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        try {
                            data = (Map<String, Object>) dataSnapshot.getValue();
                            postAdapter.setPostMap(convertMapData(data));
                            postAdapter.notifyDataSetChanged();
                            postAdapter.getFilter().filter("");
                            Log.d("Database", "Data found");
                        } catch (Exception ignored) {

                        }
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {
                        Log.e("error", databaseError.getMessage());
                    }
                });
        //adatbazis valtozasok(CRUD) kezelese
        ref.addChildEventListener(new ChildEventListener() {
            @Override
            @SuppressWarnings({"unchecked"})
            public void onChildAdded(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {
                postAdapter.addNewElement(dataSnapshot.getKey(),
                        convertSingleData((Map<String, Object>) Objects.requireNonNull(dataSnapshot.getValue())));
                postAdapter.notifyDataSetChanged();
                postAdapter.getFilter().filter(search.getText());
            }

            @Override
            @SuppressWarnings({"unchecked"})
            public void onChildChanged(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {
                postAdapter.changeElement(dataSnapshot.getKey(),
                        convertSingleData((Map<String, Object>) Objects.requireNonNull(dataSnapshot.getValue())));
                postAdapter.notifyDataSetChanged();
                postAdapter.getFilter().filter(search.getText());
            }

            @Override
            @SuppressWarnings({"unchecked"})
            public void onChildRemoved(@NonNull DataSnapshot dataSnapshot) {
                postAdapter.deleteElement(dataSnapshot.getKey());
                postAdapter.notifyDataSetChanged();
                postAdapter.getFilter().filter(search.getText());
            }

            @Override
            public void onChildMoved(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    //tobb adatbazis elem konvertalasa Map<String, Object> tipusuva
    @SuppressWarnings({"unchecked"})
    private Map<String, Object> convertMapData(Map<String, Object> data) {
        Map<String, Object> returnMap = new LinkedHashMap<>();

        for (Map.Entry<String, Object> entry : data.entrySet()) {

            Map singlePost = (Map) entry.getValue();
            returnMap.put(entry.getKey(), convertSingleData(singlePost));
        }
        return returnMap;
    }

    //egy adatbazis elem konvertalasa Post tipusuva
    @SuppressWarnings({"unchecked"})
    private Post convertSingleData(Map<String, Object> data) {

        return new Post((String) data.get("userID"),
                (String) data.get("title"),
                (String) data.get("details"),
                (String) data.get("phone"),
                (String) data.get("email"),
                (ArrayList<String>) data.get("photoUrl"),
                (Long) data.get("date"));
    }
}
